#!/usr/bin/python3


import argparse
import pandas as pd
import matplotlib.pyplot as plt
from pyncher import (
    filter_dates,
    basic_report,
    plot_report,
    get_df_string,
    get_dates_string,
    header,
)


def main():
    """Entry point for CLI"""
    description = """Pyncher - simple budget report"""
    parser = argparse.ArgumentParser(prog="pyncher", description=description)
    parser.add_argument("csv", help="budget csv file")
    parser.add_argument("-f", "--from", dest="start", help="initial date of report")
    parser.add_argument("-t", "--to", dest="end", help="final date of report")
    parser.add_argument("-p", "--plot", action="store_true", help="plot results")
    parser.add_argument("-d", "--decimal", default=",", help="decimal separator")
    parser.add_argument("-s", "--save", help="file name for results")
    args = parser.parse_args()

    df = pd.read_csv(args.csv, parse_dates=[0], decimal=args.decimal)
    df.sort_values(by=df.columns[0], inplace=True)
    df = filter_dates(df, args.start, args.end)
    start_str, end_str = get_dates_string(df, args.start, args.end)
    report = basic_report(df)

    if args.plot:
        fig = plot_report(report, start_str, end_str)

        if args.save:
            plt.savefig(f"{args.save}")

        plt.show()

    else:
        head = header(start_str, end_str)
        rep = get_df_string(report)
        print(head)
        print(rep)

        if args.save:
            f = open(f"{args.save}", "w")
            f.write(head + "\n")
            f.write(rep)
            f.close()


if __name__ == "__main__":
    main()
