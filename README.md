# Pyncher

## Simple budget report

After a long time of manually wrangling spreadsheets to get basic summaries, I've decided to create a simple package for that.

The basic idea of pyncher is to get a simple budget overview.
It outputs a table with absolute and relative totals per category.

It expects a budget CSV file with columns `date`, `value` and `description`; at the moment these are hard-coded.
By default, it outputs a report of the last 30 days.
This can be changed with `-f (--from)` or `-t (--to)`, specifying start and end dates, respectively.

The default plot reflects the summary and is a simple bar plot with totals per category and percentages as text at the base of the bars.

