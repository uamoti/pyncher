#!/usr/bin/python3

import unittest
import pandas as pd
from src.pyncher.pyncher import header, filter_dates, get_dates_string, basic_report


class TestPyncher(unittest.TestCase):
    def test_header(self):
        start = "01-03-2023"
        end = "15-04-2023"
        name = "PYNCHER REPORT"
        expected = (
            "=" * 30
            + "\n"
            + f"{name:^30}\n"
            + f'{"-" * len(name):^30}\n'
            + f'{"Start: " + start:^30}\n'
            + f'{"End: " + end:^30}\n'
            + "=" * 30
        )
        result = header(start, end)
        self.assertEqual(expected, result)

    def test_end_before_start(self):
        df = pd.read_csv("tests/df_2022-11.csv", parse_dates=[0], decimal=",")
        start = "2022-11-15"
        end = "2022-11-10"
        with self.assertRaises(ValueError):
            filter_dates(df, start, end)

    def test_filter_dates_no_start_nor_end(self):
        df = pd.read_csv("tests/df_2022-11.csv", header=0, parse_dates=[0], decimal=",")
        result = filter_dates(df)
        comparison = pd.testing.assert_frame_equal(df, result)
        self.assertIsNone(comparison)

    def test_filter_dates_start_only(self):
        df = pd.read_csv("tests/df_2022-11.csv", header=0, parse_dates=[0], decimal=",")
        df2 = df[df.date >= "2022-11-10"]
        result = filter_dates(df, start="2022-11-10")
        comparison = pd.testing.assert_frame_equal(df2, result)
        self.assertIsNone(comparison)

    def test_filter_dates_end_only(self):
        df = pd.read_csv("tests/df_2022-11.csv", header=0, parse_dates=[0], decimal=",")
        df2 = df[df.date <= "2022-11-20"]
        result = filter_dates(df, end="2022-11-20")
        comparison = pd.testing.assert_frame_equal(df2, result)
        self.assertIsNone(comparison)

    def test_get_dates_string_no_start_nor_end(self):
        df = pd.read_csv("tests/df_2022-11.csv", header=0, parse_dates=[0], decimal=",")
        start, end = get_dates_string(df)
        expected = ("03-11-2022", "30-11-2022")
        self.assertEqual(expected, (start, end))

    def test_get_dates_string_start_only(self):
        df = pd.read_csv("tests/df_2022-11.csv", header=0, parse_dates=[0], decimal=",")
        start, end = get_dates_string(df, start="2022-11-15")
        expected = ("15-11-2022", "30-11-2022")
        self.assertEqual(expected, (start, end))

    def test_get_dates_string_end_only(self):
        df = pd.read_csv("tests/df_2022-11.csv", header=0, parse_dates=[0], decimal=",")
        start, end = get_dates_string(df, end="2022-11-20")
        expected = ("03-11-2022", "20-11-2022")
        self.assertEqual(expected, (start, end))


if __name__ == "__main__":
    unittest.main()
